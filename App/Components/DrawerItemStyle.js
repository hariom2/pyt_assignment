import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    logo_container: {
        backgroundColor: '#858585',
        // width:'45rem',
        padding: '15rem',
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: '30rem'
    },
    user_name_style: {
        fontFamily: 'Heebo-Medium',
        fontSize: '13rem',
        color: '#373f4f',
        textAlign: 'center',
    },
    last_login: {
        fontFamily: 'Heebo-Regular',
        fontSize: '9rem',
        color: '#858585',
        textAlign: 'center',
    },
    drawer_container: {
        padding: '20rem',
        height: '100%'
    },
    version_container: {
        position: 'absolute',
        bottom: 20,
        width: '100%'
    },
    row: {
        flexDirection: 'row',
    },
    v_center: {
        alignItems: 'center',
        flex: 1
    },
    icon_image: {
        height: '20rem',
        width: '20rem',
        marginLeft: '20rem'
    },
    menu_container: {
        marginTop: '40rem',
    },
    drawer_name: {
        color: '#373f4f',
        fontFamily: 'Heebo-Medium',
        fontSize: '13rem'
    },
})