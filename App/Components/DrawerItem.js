import React, { useState, useRef } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import styles from "./DrawerItemStyle";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import { getRsvp } from '../Stores/rsvpStore'

const DrawerItem = ({ navigation }) => {
    const dispatch = useDispatch();

    const home = () => {
        navigation.navigate('HomeScreen');
        dispatch(getRsvp());
    };
    const user = () => {
        navigation.navigate('UserSearchScreen');
        dispatch(getRsvp());
    };
    const report = () => {
        navigation.navigate('ReportsScreen');
        dispatch(getRsvp());
    };

    return (
        <>
            <View style={styles.drawer_container}>
                <TouchableOpacity style={[styles.row, styles.menu_container]} onPress={home}>
                    <Text style={styles.drawer_name}> Home </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.row, styles.menu_container]} onPress={user}>
                    <Text style={styles.drawer_name}> Find RSVP'd </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.row, styles.menu_container]} onPress={report}>
                    <Text style={styles.drawer_name}> Reports </Text>
                </TouchableOpacity>
            </View>
        </>
    )
}

export default DrawerItem;