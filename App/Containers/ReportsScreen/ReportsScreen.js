
import React from 'react'
import {
    AppRegistry,
    StyleSheet,
    ScrollView,
    Text,
    View,
    processColor
} from 'react-native';
import { BarChart, PieChart } from 'react-native-charts-wrapper'
import { connect } from 'react-redux'

import styles from './ReportsScreenStyle'

class ReportsScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            rsvp: props.rsvp,
            pie: {
                detail: {
                    time_value_list: [2017],
                    legend_list: ['13-18', '18-25', '25+'],
                }
            },
            student: {
                detail: {
                    time_value_list: ['count'],
                    legend_list: ['Students', 'Professionals'],
                    dataset: {
                        'Students': { 'count': 17 },
                        'Professionals': { 'count': 29 },
                    }
                }
            },
            bar: {
                title: 'No Of People By Localites',
                detail: {
                    legend_list: ['city'],
                }
            },
        }
    }

    getRandomColor() {
        var letters = '0123456789ABCDEF'
        var color = '#'
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)]
        }
        return color
    }

    renderPie() {
        const time = this.state.pie.detail.time_value_list
        const legend = this.state.pie.detail.legend_list

        const firstAgeGrp = this.state.rsvp.filter(rs => rs.age >= 13 && rs.age <= 18)
        const secondAgeGrp = this.state.rsvp.filter(rs => rs.age > 18 && rs.age <= 25)
        const thirdAgeGrp = this.state.rsvp.filter(rs => rs.age > 25)
        const dataset = {
            '13-18': { '2017': firstAgeGrp.length },
            '18-25': { '2017': secondAgeGrp.length },
            '25+': { '2017': thirdAgeGrp.length }
        }
        console.log('dataset', dataset)
        var dataSetsValue = []
        var dataStyle = {}
        var legendStyle = {}
        var descStyle = {}
        var xAxisStyle = {}
        var chooseStyle = {}
        var valueLegend = []
        var colorLegend = []

        legend.map((legendValue) => {
            time.map((timeValue) => {
                const datasetValue = dataset[legendValue]
                const datasetTimeValue = datasetValue[timeValue]

                valueLegend.push({ value: parseInt(datasetTimeValue), label: legendValue })
            })
            colorLegend.push(processColor(this.getRandomColor()))
        })

        const datasetObject = {
            values: valueLegend,
            label: '',
            config: {
                colors: colorLegend,
                valueTextSize: 20,
                valueTextColor: processColor('green'),
                sliceSpace: 5,
                selectionShift: 13
            }
        }
        dataSetsValue.push(datasetObject)

        legendStyle = {
            enabled: true,
            textSize: 12,
            form: 'CIRCLE',
            position: 'BELOW_CHART_RIGHT',
            wordWrapEnabled: true
        }
        dataStyle = {
            dataSets: dataSetsValue
        }
        descStyle = {
            text: '',
            textSize: 15,
            textColor: processColor('darkgray')
        }

        return (
            <PieChart
                style={styles.bar}
                chartDescription={descStyle}
                data={dataStyle}
                legend={legendStyle}
                highlights={[{ x: 2 }]} />
        )
    }

    renderPieStudent() {
        const time = this.state.student.detail.time_value_list
        const legend = this.state.student.detail.legend_list

        const students = this.state.rsvp.filter(rs => rs.profession === "Student");
        const employes = this.state.rsvp.filter(rs => rs.profession === "Employed");

        const dataset = {
            'Students': { 'count': students.length },
            'Professionals': { 'count': employes.length },
        }

        var dataSetsValue = []
        var dataStyle = {}
        var legendStyle = {}
        var descStyle = {}
        var xAxisStyle = {}
        var chooseStyle = {}
        var valueLegend = []
        var colorLegend = []

        legend.map((legendValue) => {
            time.map((timeValue) => {
                const datasetValue = dataset[legendValue]
                const datasetTimeValue = datasetValue[timeValue]

                valueLegend.push({ value: parseInt(datasetTimeValue), label: legendValue })
            })
            colorLegend.push(processColor(this.getRandomColor()))
        })

        const datasetObject = {
            values: valueLegend,
            label: '',
            config: {
                colors: colorLegend,
                valueTextSize: 20,
                valueTextColor: processColor('green'),
                sliceSpace: 5,
                selectionShift: 13
            }
        }
        dataSetsValue.push(datasetObject)

        legendStyle = {
            enabled: true,
            textSize: 12,
            form: 'CIRCLE',
            position: 'BELOW_CHART_RIGHT',
            wordWrapEnabled: true
        }
        dataStyle = {
            dataSets: dataSetsValue
        }
        descStyle = {
            text: '',
            textSize: 15,
            textColor: processColor('darkgray')
        }

        return (
            <PieChart
                style={styles.bar}
                chartDescription={descStyle}
                data={dataStyle}
                legend={legendStyle}
                highlights={[{ x: 2 }]} />
        )
    }

    renderBar() {

        const style1 = {
            barWidth: 0.1,
            groupSpace: 0.2
        }
        const style2 = {
            barWidth: 0.2,
            groupSpace: 0.1
        }
        const style3 = {
            barWidth: 0.3,
            groupSpace: 0.2
        }

        let localityName = [];
        let calculateDataSet = {}

        const { rsvp } = this.state;
        let filterByLocatily = [{ locality: 'Goochland', detail: [] }]
        rsvp.map(rs => {
            const isPresent = filterByLocatily.filter(loc => rs.locality === loc.locality);
            if (isPresent.length === 0) {
                localityName.push(rs.locality)
                filterByLocatily.push({
                    locality: rs.locality,
                    detail: [rs]
                })
            } else {
                let getIndex
                filterByLocatily.find((loc, index) => {
                    if (rs.locality === loc.locality) {
                        console.log(index)
                        getIndex = index
                    }
                })
                filterByLocatily[getIndex].detail.push(rs)
            }
        })
        filterByLocatily.map(loc => {
            calculateDataSet[loc.locality] = loc.detail.length
        })
        const time = localityName
        const legend = this.state.bar.detail.legend_list
        const dataset = { city: calculateDataSet }
        console.log('rsvp in bar', calculateDataSet)

        var dataSetsValue = []
        var dataStyle = {}
        var legendStyle = {}
        var descStyle = {}
        var xAxisStyle = {}
        var chooseStyle = {}
        var valueLegend = []
        var colorLegend = []

        if (legend.length === 4) {
            chooseStyle = style1
        } else if (legend.length === 3) {
            chooseStyle = style2
        } else if (legend.length === 2) {
            chooseStyle = style3
        }

        legend.map((legendValue) => {
            var valueLegend = []

            time.map((timeValue) => {
                const datasetValue = dataset[legendValue]
                const datasetTimeValue = datasetValue[timeValue]

                valueLegend.push(parseInt(datasetTimeValue))
            })

            const datasetObject = {
                values: valueLegend,
                label: legendValue,
                config: {
                    drawValues: false,
                    colors: [processColor(this.getRandomColor())]
                }
            }
            dataSetsValue.push(datasetObject)
        })

        legendStyle = {
            enabled: true,
            textSize: 14,
            form: 'SQUARE',
            formSize: 14,
            xEntrySpace: 10,
            yEntrySpace: 5,
            wordWrapEnabled: true
        }
        dataStyle = {
            dataSets: dataSetsValue,
            config: {
                barWidth: chooseStyle.barWidth, // 0.1
                group: {
                    fromX: 0,
                    groupSpace: chooseStyle.groupSpace, // 0.2
                    barSpace: 0.1
                }
            }
        }
        xAxisStyle = {
            valueFormatter: time,
            granularityEnabled: true,
            granularity: 1,
            axisMaximum: 5,
            axisMinimum: 0,
            centerAxisLabels: true
        }

        return (
            <BarChart
                style={styles.bar}
                xAxis={xAxisStyle}
                chartDescription={{ text: '' }}
                data={dataStyle}
                legend={legendStyle}
                drawValueAboveBar={false}
            />
        )
    }

    render() {
        const { rsvp } = this.state
        let numberOfGuest = 0;
        rsvp.map(rs => {
            numberOfGuest = rs.nog + numberOfGuest
        })
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.title}>
                    Average group size of people attending the event
                </Text>
                <Text style={styles.title}>
                    {numberOfGuest}
                </Text>


                <Text style={styles.title}>
                    Number of people(age range)
                </Text>
                {this.props.rsvp.length > 0 && this.renderPie()}


                <Text style={styles.title}>
                    Number of people by localities
                </Text>
                {this.props.rsvp.length > 0 && this.renderBar()}


                <Text style={styles.title}>
                    Professionals & students
                </Text>
                {this.props.rsvp.length > 0 && this.renderPieStudent()}

            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    return { rsvp: state.rsvpReducer.rsvp }
}

export default connect(mapStateToProps)(ReportsScreen)
