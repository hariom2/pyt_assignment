import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: '#F5FCFF',
    },
    title: {
        marginTop: 10,
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    bar: {
        marginTop: 10,
        height: '250rem',
        width: '250rem',
        padding: 10,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
})