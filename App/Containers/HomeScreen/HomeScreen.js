import React, { useEffect, useState } from 'react';
import { Pressable, Text, TextInput, TouchableOpacity, View, Picker, Alert } from 'react-native';
import styles from './HomeScreenStyle';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux'
import { postRsvp, getRsvp } from '../../Stores/rsvpStore'

const HomeScreen = () => {
    const dispatch = useDispatch()
    const [showDatePicker, setShowDatePicker] = useState(false);
    const [date, setDate] = useState(new Date());
    const [name, setName] = useState();
    const [age, setAge] = useState('13-18');
    const [locality, setLocality] = useState();
    const [profession, setProfession] = useState('Student');
    const [nog, setNog] = useState(1);
    const [address, setAddress] = useState();
    const [isDisable, setIsDisable] = useState(false);
    useEffect(() => {
        dispatch(getRsvp());
    }, [])


    const rsvpSubmitHandler = () => {
        if (name && locality) {
            setIsDisable(true)
            let rsvpPayload = {
                "name": name,
                "age": age,
                "dob": JSON.stringify(date),
                "profession": profession,
                "locality": locality,
                "nog": nog,
                "address": address
            }
            dispatch(postRsvp(rsvpPayload));
        } else {
            Alert.alert(
                "Invalid Data",
                "Please Enter Name and Locality",
                [
                    {
                        text: "Cancel",
                        onPress: () => console.log("Cancel Pressed"),
                        style: "cancel"
                    },
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        }
    }

    return (
        <View style={styles.screen_container}>
            {/* HomeScreen Banner */}
            <View style={styles.banner}>
                <Text style={styles.secondry_heading}>Hey There!</Text>
                <Text style={styles.secondry_sub_heading}>We find perfect Online event for you...</Text>
            </View>

            {/* HomeScreen Form */}
            <View style={styles.form_container}>
                <View style={styles.w_100}>
                    <Text style={styles.primary_sub_heading}>Full Name</Text>
                    <TextInput
                        onChangeText={(text) => { setName(text); setIsDisable(false); }}
                        style={styles.input_style}
                        placeholder="Full Name" />
                </View>
                <View style={styles.w_48}>
                    <Text style={styles.primary_sub_heading}>Age</Text>
                    <View style={styles.input_style}>
                        <Picker
                            accessibilityLabel='Type of data'
                            selectedValue={age}
                            onValueChange={(itemValue, itemIndex) => {
                                setAge(itemValue)
                                setIsDisable(false);
                            }}>
                            <Picker.Item label="13-18" value="13-18" />
                            <Picker.Item label="18-25" value="18-25" />
                            <Picker.Item label="25+" value="25+" />
                        </Picker>
                    </View>
                </View>
                <View style={styles.w_48}>
                    <Text style={styles.primary_sub_heading}>Date of birth</Text>
                    <Pressable onPress={() => { setShowDatePicker(!showDatePicker); setIsDisable(false); }}>
                        <View pointerEvents="none">
                            <TextInput
                                pickerStyleType={styles.input_style}
                                value={moment(date).format('DD-MMMM-YYYY')} />
                        </View>
                        {
                            showDatePicker &&
                            (
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    value={new Date()}
                                    mode='date'
                                    display="default"
                                    onChange={selectedDate => { setDate(selectedDate.nativeEvent.timestamp); setShowDatePicker(false) }}
                                />
                            )
                        }
                    </Pressable>
                </View>
                <View style={styles.w_48}>
                    <Text style={styles.primary_sub_heading}>Locality</Text>
                    <TextInput
                        style={styles.input_style}
                        onChangeText={(text) => { setLocality(text); setIsDisable(false); }}
                        placeholder="Locality" />
                </View>
                <View style={styles.w_48}>
                    <Text style={styles.primary_sub_heading}>Number of Guests</Text>
                    <View style={styles.input_style}>
                        <Picker
                            accessibilityLabel='Type of data'
                            selectedValue={nog}
                            onValueChange={(itemValue, itemIndex) => {
                                setNog(itemValue);
                                setIsDisable(false);
                            }}>
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                        </Picker>
                    </View>
                </View>
                <View style={styles.w_100}>
                    <Text style={styles.primary_sub_heading}>Profession</Text>
                    <View style={styles.input_style}>
                        <Picker
                            accessibilityLabel='Type of data'
                            selectedValue={profession}
                            onValueChange={(itemValue, itemIndex) => {
                                setProfession(itemValue);
                                setIsDisable(false);
                            }}>
                            <Picker.Item label="Student" value="Student" />
                            <Picker.Item label="Employed" value="Employed" />
                        </Picker>
                    </View>
                </View>
                <View style={styles.w_100}>
                    <Text style={styles.primary_sub_heading}>Address</Text>
                    <TextInput
                        onChangeText={(text) => { setAddress(text); setIsDisable(false); }}
                        style={styles.input_style}
                        placeholder="Address" />
                </View>
                <View style={styles.w_100}>
                    <TouchableOpacity onPress={rsvpSubmitHandler} style={isDisable ? styles.disable_btn : styles.primary_btn}>
                        <Text style={styles.primary_btn_text}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default HomeScreen