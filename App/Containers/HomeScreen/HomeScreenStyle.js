import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    screen_container: {
        padding: '5rem',
        backgroundColor: '#fff',
        height: '100%'
    },
    banner: {
        backgroundColor: '#FEECE4',
        height: '60rem',
        borderRadius: '6rem',
        justifyContent: 'center',
        paddingLeft: '15rem'
    },
    secondry_heading: {
        fontSize: '15rem',
        fontWeight: '700'
    },
    secondry_sub_heading: {
        fontSize: '10rem',
        fontWeight: '600',
        color: '#7B7B7B'
    },
    form_container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        paddingTop: '10rem',
        paddingLeft: '5rem',
        paddingRight: '5rem'
    },
    w_100: {
        width: '100%',
    },
    w_48: {
        width: '48%',
    },
    input_style: {
        borderColor: '#E5E5E5',
        borderWidth: '1rem',
        borderRadius: '6rem',
        marginTop: '5rem',
        marginBottom: '5rem',
        paddingLeft: '15rem'
    },
    primary_btn: {
        backgroundColor: '#F17636',
        height: '30rem',
        width: '100rem',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft:'auto',
        borderRadius:'6rem'
    },
    disable_btn: {
        backgroundColor: '#858585',
        height: '30rem',
        width: '100rem',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft:'auto',
        borderRadius:'6rem'
    },
    primary_btn_text:{
        color:'#ffffff'
    }
})

// import EStyleSheet from 'react-native-extended-stylesheet';

// export default EStyleSheet.create({

// })

