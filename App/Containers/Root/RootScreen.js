import React from 'react';
import { SafeAreaView, Text } from 'react-native';
import { Drawernav } from "./../../Navigators/AppNavigator";

const RootScreen = () => {
    return (
        <>
            <Drawernav />
        </>
    )
}

export default RootScreen;
