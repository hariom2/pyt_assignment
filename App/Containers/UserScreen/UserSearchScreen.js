import React, { useState } from 'react';
import { Text, View, TextInput, TouchableOpacity, Pressable, ScrollView, Modal } from 'react-native';
import styles from './UserStyle';
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'

const UserSearchScreen = () => {
    const [modalVisible, setModalVisible] = useState(false);

    const rsvp = useSelector(state => state.rsvpReducer.rsvp);
    const [rsvpList, setRsvpList] = useState([]);
    const [selectedRsvp, setSelectedRsvp] = useState([]);
    const rsvpSearchHandler = (text) => {
        console.log(rsvp);
        const res = rsvp && rsvp.filter(val => {
            let resLen = val.name.toLowerCase().indexOf(text);
            let resLenLocality = val.locality.toLowerCase().indexOf(text);
            if (resLen >= 0 || resLenLocality >= 0) {
                return val
            } else {
                return
            }
        });
        text.length > 0 && res && setRsvpList(res);
    }

    return (
        <View style={styles.screen_container}>
            {/* Banner */}
            <View style={styles.banner}>
                <TextInput
                    style={styles.input_style}
                    onChangeText={text => rsvpSearchHandler(text)}
                    placeholder="Search RSVP'd by Name & Locality" />
            </View>
            {/* User Card */}
            <ScrollView>
                {
                    rsvpList
                        ? rsvpList.map(rsvp => <UserCard
                            setModalVisible={setModalVisible}
                            setSelectedRsvp={setSelectedRsvp}
                            key={rsvp.name}
                            rsvp={rsvp} />)
                        : null
                }
            </ScrollView>

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <Pressable style={styles.model_container} onPress={() => setModalVisible(false)}>
                    <View style={styles.model}>
                        <Pressable
                            style={[styles.button, styles.buttonClose]}
                            onPress={() => setModalVisible(!modalVisible)}
                        >
                            <UserDetail selectedRsvp={selectedRsvp} />
                        </Pressable>
                    </View>
                </Pressable>
            </Modal>
        </View>
    )
}

const UserDetail = (props) => {
    const { selectedRsvp } = props
    return (
        <View>
            <Text style={styles.disable_text}>Name: <Text style={styles.enable_text}>{selectedRsvp.name}</Text></Text>
            <Text style={styles.disable_text}>Profession: <Text style={styles.enable_text}>{selectedRsvp.profession}</Text></Text>
            <Text style={styles.disable_text}>No of Guest: <Text style={styles.enable_text}>{selectedRsvp.nos}</Text></Text>
            <Text style={styles.disable_text}>Locality: <Text style={styles.enable_text}>{selectedRsvp.locality}</Text></Text>
            <Text style={styles.disable_text}>Date of birth: <Text style={styles.enable_text}>{moment(selectedRsvp.dob).format('DD-MMM-YYYY')}</Text></Text>
            <Text style={styles.disable_text}>Age: <Text style={styles.enable_text}>{selectedRsvp.age}</Text></Text>
            <Text style={styles.disable_text}>Address: <Text style={styles.enable_text}>{selectedRsvp.address}</Text></Text>
        </View>
    )
}

const UserCard = (props) => {
    const { rsvp, setModalVisible, setSelectedRsvp } = props;
    return (
        <Pressable onPress={() => { setModalVisible(true); setSelectedRsvp(rsvp); }}>
            <View style={styles.card} >
                <View style={styles.basic_info_container}>
                    <View style={styles.profile}>
                        <Text style={styles.profile_text}>{rsvp.name.toUpperCase().substring(0, 2)}</Text>
                    </View>
                    <View style={styles.name}>
                        <Text style={styles.name_text}>{rsvp.name}</Text>
                        <Text style={styles.location_text}>{rsvp.locality}</Text>
                    </View>
                </View>
                <View>
                    <Text style={styles.prof_text}>{rsvp.profession}</Text>
                </View>
            </View>
        </Pressable>
    )
}

export default UserSearchScreen
