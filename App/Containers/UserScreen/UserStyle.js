import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    screen_container: {
        padding: '5rem',
        backgroundColor: '#fff',
        height: '100%'
    },
    banner: {
        backgroundColor: '#FEECE4',
        height: '100rem',
        borderRadius: '6rem',
        justifyContent: 'center',
        paddingLeft: '15rem',
        paddingRight: '15rem'
    },
    model: {
        backgroundColor: '#fff',
        padding: '20rem',
        borderRadius: '6rem',
        justifyContent: 'center',
        paddingLeft: '15rem',
        paddingRight: '15rem',
        width: '90%',
        alignItems: 'center'
    },
    model_container: {
        height: '100%',
        width: '100%',
        backgroundColor: '#0000007a',
        alignItems: 'center',
        justifyContent: 'center'
    },
    secondry_heading: {
        fontSize: '15rem',
        fontWeight: '700'
    },
    secondry_sub_heading: {
        fontSize: '10rem',
        fontWeight: '600',
        color: '#7B7B7B'
    },
    input_style: {
        borderColor: '#fff',
        borderWidth: '1rem',
        borderRadius: '6rem',
        marginTop: '10rem',
        marginBottom: '5rem',
        paddingLeft: '15rem',
        backgroundColor: '#ffffff'
    },
    card: {
        borderColor: '#EEEEEE',
        borderWidth: '1rem',
        borderRadius: '6rem',
        marginTop: '10rem',
        padding: '17rem',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    basic_info_container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    profile: {
        borderColor: '#7B7B7B',
        borderWidth: '1rem',
        borderRadius: '20rem',
        height: '35rem',
        width: '35rem',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: '12rem'
    },
    profile_text: {
        fontSize: '12rem',
        color: '#7B7B7B',

    },
    name_text: {
        fontSize: '12rem',
        color: '#000000',
    },
    location_text: {
        fontSize: '10rem',
        color: '#7b7b7b',
    },
    prof_text: {
        fontSize: '10rem',
        color: '#F17636',
    },
    disable_text: {
        color: '#858585',
        fontSize: '13rem',
        marginTop:'7rem'
    },
    enable_text: {
        color: '#000',
        fontSize: '13rem'
    }
})