import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerItem from '../Components/DrawerItem';
import { navigationRef } from '../Services/navService';
import { useDispatch, useSelector } from "react-redux";
import styles from "./AppNavigatorStyle";
import { navigate } from '../Services/navService';

import HomeScreen from '../Containers/HomeScreen/HomeScreen';
import SplashScreen from '../Containers/SplashScreen/SplashScreen';
import UserSearchScreen from '../Containers/UserScreen/UserSearchScreen'
import ReportsScreen from '../Containers/ReportsScreen/ReportsScreen';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function CustomDrawerContent(props) {
    return <DrawerItem {...props} />;
}

export const Drawernav = () => {
    return (
        <NavigationContainer independent={true} ref={navigationRef}>
            <Drawer.Navigator
                drawerContent={(props) => <CustomDrawerContent {...props} />}
            >
                <Drawer.Screen
                    name="SplashScreen"
                    options={{ gestureEnabled: true, swipeEnabled: true }}
                >
                    {(props) => <StackNav {...props} />}
                </Drawer.Screen>
            </Drawer.Navigator>
        </NavigationContainer>
    );
};

const StackNav = (props) => {
    return (
        <Stack.Navigator initialRouteName={'HomeScreen'}>
            <Stack.Screen
                name="HomeScreen"
                component={HomeScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="UserSearchScreen"
                component={UserSearchScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="ReportsScreen"
                component={ReportsScreen}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    )
}