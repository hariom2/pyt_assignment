import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    row_view: {
        flexDirection: 'row',
        padding: '10rem',
    },
    container: {
        justifyContent: 'center', //Centered vertically
        alignItems: 'center', // Centered horizontally
        flex: 1
    },
    header_container: {
        justifyContent: 'space-between', //Centered vertically
        width: '100%',
        alignItems: 'center', // Centered horizontally
    },
    hemberger_icon: {
        width: 25,
        height: 20,
        marginRight: '5 rem'
    },
    logout_icon: {
        width: 20,
        height: 20,
        marginLeft: '6 rem'
    },
    header_heading: {
        color: '#002C33',
        fontSize: '12rem',
        fontFamily: 'Heebo-Regular',
    },

})