import axios from 'axios';
import { URLs } from '../Constant'

export const getRsvpService = async () => {
    const url = URLs.rsvpUrl
    const response = await axios
        .get(url)
        .catch(err => err.response);
    console.log(response);
    return response.data
}

export const postRsvpService = async (payload) => {
    const url = URLs.rsvpUrl
    const response = await axios
        .post(url, payload)
        .catch(err => err.response);
    return response
}
