
import React from 'react';
import { Dimensions, Text } from 'react-native';
import RootScreen from "./Containers/Root/RootScreen";
import { Provider } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import { store } from "./Stores/createStore";

const entireScreenWidth = Dimensions.get('window').width;

function App() {
    EStyleSheet.build({ $rem: entireScreenWidth / 414 });
    return (
        <>
            <Provider store={store}>
            <RootScreen />
            </Provider>
        </>
    );
}

export default App;
