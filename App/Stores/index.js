import { combineReducers } from 'redux';
import { splashReducer } from "./splashStore";
import { rsvpReducer } from './rsvpStore'

export default combineReducers({
    splashReducer,
    rsvpReducer
})