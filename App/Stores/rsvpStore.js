import { createAction } from 'redux-actions';

export const GET_RSVP = 'GET_RSVP';
export const STORE_RSVP = 'STORE_RSVP';
export const POST_RSVP = 'POST_RSVP';
export const UPDATE_RSVP = 'UPDATE_RSVP';
export const SHOW_ALERT = 'SHOW_ALERT';
export const HIDE_ALERT = 'HIDE_ALERT';

export const getRsvp = createAction(GET_RSVP);
export const storeRsvp = createAction(STORE_RSVP);
export const postRsvp = createAction(POST_RSVP);
export const updateRsvp = createAction(UPDATE_RSVP);
export const showAlert = createAction(SHOW_ALERT);
export const hideAlert = createAction(HIDE_ALERT);

const INITIAL_STATE = {
    rsvp: [],
    alert: false
};

export const rsvpReducer = (state = INITIAL_STATE, action = {}) => {
    let payLoad = action.payload;
    switch (action.type) {
        case STORE_RSVP:
            return {
                ...state,
                rsvp: payLoad
            }
        case UPDATE_RSVP:
            return {
                ...state,
                rsvp: state.rsvp.concat(payLoad)
            }
        case SHOW_ALERT:
            return {
                ...state,
                alert: true
            }
        case HIDE_ALERT:
            return {
                ...state,
                alert: false
            }
        default:
            return state;
    }
}