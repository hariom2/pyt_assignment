import { createAction } from 'redux-actions';

export const SHOW_SPLASH_STORE = 'SHOW_SPLASH_STORE';
export const HIDE_SPLASH_STORE = 'HIDE_SPLASH_STORE';

export const showSplash = createAction(SHOW_SPLASH_STORE);
export const hideSplash = createAction(HIDE_SPLASH_STORE);

const INITIAL_STATE = {
    msg: '',
    type: '',
    isShow: false,
};

export const splashReducer = (state = INITIAL_STATE, action = {}) => {
    let payLoad = action.payload;
    switch (action.type) {
        case SHOW_SPLASH_STORE:
            return {
                ...state,
                msg: payLoad.msg,
                type: payLoad.splashType,
                isShow: true
            }
        case HIDE_SPLASH_STORE:
            return {
                ...state,
                msg: '',
                type: '',
                isShow: false
            }
        default:
            return state;
    }
}