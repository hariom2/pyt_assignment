import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import reducer from './index';
import { rootSagas } from '../Sagas/rootSaga'

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(reducer, applyMiddleware(sagaMiddleware, createLogger()));
sagaMiddleware.run(rootSagas);
