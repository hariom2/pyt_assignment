import { takeEvery, all } from 'redux-saga/effects';

import { GET_RSVP, POST_RSVP } from '../Stores/rsvpStore';
import { getRsvp, postRsvp } from './rsvpSaga';

export function* rootSagas() {
    yield all([
        takeEvery(GET_RSVP, getRsvp),
        takeEvery(POST_RSVP, postRsvp)
    ]);
}