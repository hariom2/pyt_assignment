import { put, call } from 'redux-saga/effects';
import { storeRsvp, updateRsvp } from '../Stores/rsvpStore';
import { getRsvpService, postRsvpService } from '../Services/rsvpService';

export function* getRsvp(action) {
    const response = yield call(getRsvpService);
    console.log('response.data', response.data)
    if (response.data){
        yield put(storeRsvp(response))
    }
}

export function* postRsvp(action) {
    const payload = action.payload;
    const response = yield call(postRsvpService, payload);
    yield put(updateRsvp(payload));
}